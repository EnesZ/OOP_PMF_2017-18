#include <iostream>
#include <string>
#include <math.h>

using namespace std;

double funkcija(double x){
    return sin(x);
}

int main()
{
    int sirina, visina;
    double x1,x2,y1,y2, vrijednost_funkcije;
    const char OKVIR='#';
    const char F_ZNAK='*';

    do{
        cout<<"Unesite sirinu i visinu dijagrama\n(u sirinu i visinu se ubraja i okvir, zbog preglednosti sirina je max 80)"<<endl;
        cout<<"Sirina: ";
        cin>>sirina;
        cout<<"Visina: ";
        cin>>visina;
    }while(sirina<1 || visina <1);

    //zbog okvira
    sirina-=2;
    visina-=2;

    do{
        cout<<"Unesite granice za segment [x1,x2]\nx1: ";
        cin>>x1;
        cout<<"x2: ";
        cin>>x2;
    }while(x1>=x2);

    do{
        cout<<"Unesite granice za segment [y1,y2]\ny1: ";
        cin>>y1;
        cout<<"y2: ";
        cin>>y2;
    }while(y1>=y2);

    //korak
    double dx=(x2-x1)/sirina;
    double dy=(y2-y1)/visina;

    string temp(sirina+2,OKVIR);
    cout<<temp;

    if(sirina<78)
        cout<<endl;
    //inace program ce sam preci u novi red, za slucaj sa sirinom dijagrama 80

    //da se bolje vidi funkcija, konkretno za skup parametara 3 iz zadatka
    double podebljanje=0.;
    if(dy<=0.05)
        podebljanje+=dy;


    for(double i=y2; i>y1; i-=dy){

        cout<<OKVIR;

            /*
            *   Zbog ogranicene preciznosti koraka dx moze se desiti da program ispise kolonu vise
            *   U slucaju sirine 80, potpuni izgled dijagrama ce se poremeti
            *   Zbog toga uvodimo varijablu temp_brojac koja ce osigurati da broj kolona bude pocetna sirina-2
            *
            *   Naravno, da je program ispravan i bez dodatne varijable temp_brojac moze se provjeriti
            *   za sirinu 80 i recimo x1=-38 , x2=40 tako da je dx=1
            */

        int temp_brojac=0;

        for(double j=x1; j<x2; j+=dx){

            temp_brojac++;
            if(temp_brojac>sirina)
                break;

            vrijednost_funkcije= funkcija(j);

            if(i-dy<=vrijednost_funkcije && vrijednost_funkcije <i+podebljanje)
                cout<<F_ZNAK;
            else if((i-dy<=0 && 0<i)&&(j<0 && 0<=j+dx))
                cout<<'+';
            else if(i-dy<=0 && 0<i)
                cout<<'-';
            else if(j<0 && 0<=j+dx)
                cout<<'|';
            else
                cout<<' ';
        }

        cout<<OKVIR;

        if(sirina<78)
            cout<<endl;
    }
    cout<<temp<<endl;
    return 0;
}
