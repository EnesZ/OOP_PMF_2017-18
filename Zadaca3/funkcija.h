#ifndef _FUNKCIJA_H
#define _FUNKCIJA_H

#include <iostream>
#include <string>
#include <vector>

struct Parametri{

    double x1,x2,y1,y2;
    int sirina,visina;

};

extern const char OKVIR;

std::vector<std::string> funkcijaSin(Parametri const &par, char c='*');
std::vector<std::string> funkcijaCos(Parametri const &par, char c='*');
std::vector<std::string> funkcija(Parametri const &par, char c='*');
void ispisFunkcije(std::vector<std::string> const &vec);
std::vector<std::string> spojiFunkcije(std::vector<std::string> const &vec1, std::vector<std::string> const &vec2);

#endif // _FUNKCIJA_H
