#include <iostream>
#include <string>
#include <vector>
#include "funkcija.h"

using namespace std;

int main()
{
    Parametri parametri;
    do{
        cout<<"Unesite sirinu i visinu dijagrama\n(u sirinu i visinu se ubraja i okvir, zbog preglednosti sirina je max 80)"<<endl;
        cout<<"Sirina: ";
        cin>>parametri.sirina;
        cout<<"Visina: ";
        cin>>parametri.visina;
    }while(parametri.sirina<1 || parametri.visina <1);

    //zbog okvira
    parametri.sirina-=2;
    parametri.visina-=2;

    do{
        cout<<"Unesite granice za segment [x1,x2]\nx1: ";
        cin>>parametri.x1;
        cout<<"x2: ";
        cin>>parametri.x2;
    }while(parametri.x1>=parametri.x2);

    do{
        cout<<"Unesite granice za segment [y1,y2]\ny1: ";
        cin>>parametri.y1;
        cout<<"y2: ";
        cin>>parametri.y2;
    }while(parametri.y1>=parametri.y2);


    vector<string> f1,f2,f3;

    f1=funkcijaSin(parametri);
    ispisFunkcije(f1);

    f2=funkcijaCos(parametri);
    ispisFunkcije(f2);

    f3=funkcija(parametri);
    ispisFunkcije(f3);

    f1=spojiFunkcije(f1,f2);
    f1=spojiFunkcije(f1,f3);
    ispisFunkcije(f1);


    return 0;
}
