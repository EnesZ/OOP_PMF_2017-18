#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include "funkcija.h"

const char OKVIR='#';

std::vector<std::string> funkcijaSin(Parametri const &par, char c/* ='*'  */){

    std::vector<std::string> ispis;
    std::string red;
    double vrijednost_funkcije;

    //korak
    double dx=(par.x2-par.x1)/par.sirina;
    double dy=(par.y2-par.y1)/par.visina;

    std::string temp(par.sirina+2,OKVIR);
    ispis.push_back(temp);

     //da se bolje vidi funkcija, konkretno za skup parametara 3 iz zadace 1 (sada samo kopirano)
    double podebljanje=0.;
    if(dy<=0.05)
        podebljanje+=dy;


    for(double i=par.y2; i>par.y1; i-=dy){

        red=OKVIR;

            /*
            *   Zbog ogranicene preciznosti koraka dx moze se desiti da program ispise kolonu vise
            *   U slucaju sirine 80, potpuni izgled dijagrama ce se poremeti
            *   Zbog toga uvodimo varijablu temp_brojac koja ce osigurati da broj kolona bude pocetna sirina-2
            *
            *   Naravno, da je program ispravan i bez dodatne varijable temp_brojac moze se provjeriti
            *   za sirinu 80 i recimo x1=-38 , x2=40 tako da je dx=1
            *
            *   (samo kopirano iz zadace 1)
            */

        int temp_brojac=0;

        for(double j=par.x1; j<par.x2; j+=dx){

            temp_brojac++;
            if(temp_brojac>par.sirina)
                break;

            vrijednost_funkcije= sin(j);

            if(i-dy<=vrijednost_funkcije && vrijednost_funkcije <i+podebljanje)
                red+=c;
            else if((i-dy<=0 && 0<i)&&(j<0 && 0<=j+dx))
                red+='+';
            else if(i-dy<=0 && 0<i)
                red+='-';
            else if(j<0 && 0<=j+dx)
                red+='|';
            else
                red+=' ';
        }

        red+=OKVIR;

        ispis.push_back(red);

    }

    ispis.push_back(temp);
    return ispis;
}
std::vector<std::string> funkcijaCos(Parametri const &par, char c/* ='*'  */){

    std::vector<std::string> ispis;
    std::string red;
    double vrijednost_funkcije;

    //korak
    double dx=(par.x2-par.x1)/par.sirina;
    double dy=(par.y2-par.y1)/par.visina;

    std::string temp(par.sirina+2,OKVIR);
    ispis.push_back(temp);

     //da se bolje vidi funkcija, konkretno za skup parametara 3 iz zadace 1 (sada samo kopirano)
    double podebljanje=0.;
    if(dy<=0.05)
        podebljanje+=dy;


    for(double i=par.y2; i>par.y1; i-=dy){

        red=OKVIR;

            /*
            *   Zbog ogranicene preciznosti koraka dx moze se desiti da program ispise kolonu vise
            *   U slucaju sirine 80, potpuni izgled dijagrama ce se poremeti
            *   Zbog toga uvodimo varijablu temp_brojac koja ce osigurati da broj kolona bude pocetna sirina-2
            *
            *   Naravno, da je program ispravan i bez dodatne varijable temp_brojac moze se provjeriti
            *   za sirinu 80 i recimo x1=-38 , x2=40 tako da je dx=1
            *
            *   (samo kopirano iz zadace 1)
            */

        int temp_brojac=0;

        for(double j=par.x1; j<par.x2; j+=dx){

            temp_brojac++;
            if(temp_brojac>par.sirina)
                break;

            vrijednost_funkcije= cos(j);

            if(i-dy<=vrijednost_funkcije && vrijednost_funkcije <i+podebljanje)
                red+=c;
            else if((i-dy<=0 && 0<i)&&(j<0 && 0<=j+dx))
                red+='+';
            else if(i-dy<=0 && 0<i)
                red+='-';
            else if(j<0 && 0<=j+dx)
                red+='|';
            else
                red+=' ';
        }

        red+=OKVIR;

        ispis.push_back(red);

    }

    ispis.push_back(temp);
    return ispis;

}
std::vector<std::string> funkcija(Parametri const &par, char c/* ='*'  */){

    std::vector<std::string> ispis;
    std::string red;
    double vrijednost_funkcije;

    //korak
    double dx=(par.x2-par.x1)/par.sirina;
    double dy=(par.y2-par.y1)/par.visina;

    std::string temp(par.sirina+2,OKVIR);
    ispis.push_back(temp);

     //da se bolje vidi funkcija, konkretno za skup parametara 3 iz zadace 1 (sada samo kopirano)
    double podebljanje=0.;
    if(dy<=0.05)
        podebljanje+=dy;


    for(double i=par.y2; i>par.y1; i-=dy){

        red=OKVIR;

            /*
            *   Zbog ogranicene preciznosti koraka dx moze se desiti da program ispise kolonu vise
            *   U slucaju sirine 80, potpuni izgled dijagrama ce se poremeti
            *   Zbog toga uvodimo varijablu temp_brojac koja ce osigurati da broj kolona bude pocetna sirina-2
            *
            *   Naravno, da je program ispravan i bez dodatne varijable temp_brojac moze se provjeriti
            *   za sirinu 80 i recimo x1=-38 , x2=40 tako da je dx=1
            *
            *   (samo kopirano iz zadace 1)
            */

        int temp_brojac=0;

        for(double j=par.x1; j<par.x2; j+=dx){

            temp_brojac++;
            if(temp_brojac>par.sirina)
                break;

            vrijednost_funkcije= (-3)*j*j;

            if(i-dy<=vrijednost_funkcije && vrijednost_funkcije <i+podebljanje)
                red+=c;
            else if((i-dy<=0 && 0<i)&&(j<0 && 0<=j+dx))
                red+='+';
            else if(i-dy<=0 && 0<i)
                red+='-';
            else if(j<0 && 0<=j+dx)
                red+='|';
            else
                red+=' ';
        }

        red+=OKVIR;

        ispis.push_back(red);

    }

    ispis.push_back(temp);
    return ispis;
}
void ispisFunkcije(std::vector<std::string> const &vec){

    for(int i=0;i<vec.size();i++){
        std::cout<<vec.at(i);

        if(vec.at(0).size()<80)
            std::cout<<std::endl;
        //else sam ce ispisati novi red

    }

}
std::vector<std::string> spojiFunkcije(std::vector<std::string> const &vec1, std::vector<std::string> const &vec2){

    std::vector<std::string> ispis=vec1;
    std::string red;

    for(int i=0;i<vec2.size();i++){

        red=vec2.at(i);

        for(int j=0;j<red.size();j++){

            // Pretpostavimo da funkcija nije iscrtana sa znakovima ' ','-','+','|' i OKVIR jer se oni koriste za druge svrhe

            if(red.at(j)==' ' || red.at(j)=='-' || red.at(j)=='+'
                || red.at(j)=='|' || red.at(j)==OKVIR)
                    continue;
            else
                ispis.at(i).at(j)=red.at(j);
        }

    }

    return ispis;
}
