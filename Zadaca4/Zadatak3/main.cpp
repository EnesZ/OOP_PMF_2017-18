#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
    /*Zadatak 3*/

    cout << "Unesite rijeci:" << endl;

    vector<string> lista;
    string rijec;

    while(cin>>rijec && rijec!="kraj")
        lista.push_back(rijec);



    cout<<"Duzina liste je: "<<lista.size()<<endl;

    vector<char> lista1;
    string temp;


    for(int i=0;i<lista.size();i++){
        temp=lista.at(i);

        for(int j=0;j<temp.size();j++)
            lista1.push_back(temp.at(j));
    }

    cout<<"Modifikovana lista"<<endl;

    for(int i=0;i<lista1.size();i++)
        cout<<lista1.at(i)<<endl;

    cout<<"Duzina liste je: "<<lista1.size()<<endl;

    return 0;
}
