#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

struct Predmet{

    string naziv;
    int ects;
    vector<string> studenti;

};

void dodajStudente(Predmet& pr, vector<string> stud);
void dodajEcts(Predmet& pr, int i);
void dodajNaziv(Predmet& pr, string naz);

int main()
{
    /*Zadatak 2*/
    Predmet predmet;
    vector<string> vektor;
    vektor.push_back("Enes");
    vektor.push_back("Amra");
    vektor.push_back("Amila");
    vektor.push_back("Sandra");
    vektor.push_back("Anida");
    vektor.push_back("Edina");
    vektor.push_back("Amina");
    vektor.push_back("Sabina");

    dodajNaziv(predmet,"OOP");
    dodajStudente(predmet,vektor);

    try{

        dodajEcts(predmet,10);

    }catch(logic_error e){

        cout<<e.what();

    }

    cout<<"Naziv: "<<predmet.naziv<<" ects "<<predmet.ects<<endl;
    cout<<"Studenti: "<<endl;
    for(int i=0;i<predmet.studenti.size();i++)
        cout<<predmet.studenti.at(i)<<endl;

    return 0;
}
void dodajStudente(Predmet& pr, vector<string> stud){

    pr.studenti=stud;

}
void dodajEcts(Predmet& pr, int i){

    if(i<0)
        throw logic_error("Broj ects ne moze biti negativan broj");

    pr.ects=i;

}
void dodajNaziv(Predmet& pr, string naz){

    pr.naziv=naz;

}
