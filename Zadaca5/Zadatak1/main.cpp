#include <iostream>
#include "ugao.h"

using namespace std;

int main(){
    Ugao alfa(1,1,1);
    //Ugao beta = Ugao(4*PI+PI/2);
    Ugao beta = 4*PI+PI/2;
    Ugao &teta = beta;

    cout<<"alfa: "<<endl;
    alfa.IspisiKlasicno();
    alfa.Ispisi();
    cout<<endl<<"beta: "<<endl;
    teta.IspisiKlasicno();
    teta.Ispisi();

    Ugao gama = alfa.SaberiSa(beta);
    cout<<endl<<"gama = alfa + beta: "<<endl;

    gama.IspisiKlasicno();
    gama.Ispisi();

    int deg,minu,sek;
    gama.OcitajKlasicneJedinice(deg,minu,sek);
    cout<<endl<<"gama: "<<deg<<"setpeni"<<minu<<"minuta"<<sek<<"sekundi"<<endl;
    return 0;
}
