#include "ugao.h"
#include <cmath>

const double PI  =3.141592653589793238463;

Ugao::Ugao(double radijani /*= 0*/){
        //ako je negativan, postavljamo ga na osnovni period
        while(radijani<0)
            radijani+=2*PI;

        if(0 <= radijani && radijani <= 2*PI)
            _radijani=radijani;
        else
            _radijani=fmod(radijani,2*PI);
}
Ugao::Ugao(int stepeni, int minute, int sekunde){
        // minute i sekunde nema smisla zadavati kao negativne brojeve pa cemo samo uzimati njihovu apsolutnu vrijednost
        //znak stepeni se odnosi i na znak minuta i sekunda
        // -1stepen60minuta = -2stepena
        if(minute<0)
            minute*=(-1);
        if(sekunde<0)
            sekunde*=(-1);

        bool jeLiNegativan=false;
        //zbog lakseg racuna kasnije
        if(stepeni<0){
            jeLiNegativan=true;
            stepeni*=(-1);
        }

        //1 stepen = 2PI/360 radijana
        //60 minuta = 2PI/360 radijana
        //3600 sekundi = 2PI/360 radijana
        //1 sekunda = 2PI/(360*3600) radijana
        //1 sekunda = PI/(180*3600) radijana

        sekunde=sekunde+60*minute+3600*stepeni;

        if(jeLiNegativan)
            sekunde*=(-1);

        //ako su sekunde negativne svodimo ih na osnovni period dodavajuci 360stepeni tj 360*60*60= sekundi
        int period=360*60*60;
        while(sekunde<0)
            sekunde+=period;

        //ako su pozitivne i iznad 360stepeni svodimo ih na osnivni period
        if(sekunde>period)
            sekunde%=period;

        //iz gornje formule
        _radijani=PI/(180*3600)*sekunde;

}
void Ugao::Postavi(double radijani){
        while(radijani<0)
            radijani+=2*PI;

        if(0 <= radijani && radijani <= 2*PI)
            _radijani=radijani;
        else
            _radijani=fmod(radijani,2*PI);

}
void Ugao::Postavi(int stepeni, int minute, int sekunde){
        // minute i sekunde nema smisla zadavati kao negativne brojeve pa cemo samo uzimati njihovu apsolutnu vrijednost
        //znak stepeni se odnosi i na znak minuta i sekunda
        // -1stepen60minuta = -2stepena
        if(minute<0)
            minute*=(-1);
        if(sekunde<0)
            sekunde*=(-1);

        bool jeLiNegativan=false;
        //zbog lakseg racuna kasnije
        if(stepeni<0){
            jeLiNegativan=true;
            stepeni*=(-1);
        }

        //1 stepen = 2PI/360 radijana
        //60 minuta = 2PI/360 radijana
        //3600 sekundi = 2PI/360 radijana
        //1 sekunda = 2PI/(360*3600) radijana
        //1 sekunda = PI/(180*3600) radijana

        sekunde=sekunde+60*minute+3600*stepeni;

        if(jeLiNegativan)
            sekunde*=(-1);

        //ako su sekunde negativne svodimo ih na osnovni period dodavajuci 360stepeni tj 360*60*60= sekundi
        int period=360*60*60;
        while(sekunde<0)
            sekunde+=period;

        //ako su pozitivne i iznad 360stepeni svodimo ih na osnivni period
        if(sekunde>period)
            sekunde%=period;

        //iz gornje formule
        _radijani=PI/(180*3600)*sekunde;

}
double Ugao::DajRadijane() const{
        return _radijani;
}
void Ugao::OcitajKlasicneJedinice(int & stepeni, int & minute, int &sekunde){

        stepeni=DajStepene();
        minute=DajMinute();
        sekunde=DajSekunde();
}
int Ugao::DajStepene() const{
        int ukupne_sekunde=_radijani*180*3600/PI;
        //cjelobrojno dijeljenje
        return ukupne_sekunde/3600;

}
int Ugao::DajMinute() const{
        int ukupne_sekunde=_radijani*180*3600/PI;
        return (ukupne_sekunde%3600)/60;

}
int Ugao::DajSekunde() const{
        int ukupne_sekunde=_radijani*180*3600/PI;
        return ukupne_sekunde%60;

}
void Ugao::Ispisi() const{
        std::cout<<"Iznos ugla je "<<_radijani<<" radijana"<<std::endl;
}
void Ugao::IspisiKlasicno() const{

        std::cout<<"Iznos ugla je "<<DajStepene()<<"deg"<<DajMinute()<<"min"<<DajSekunde()<<"sec"<<std::endl;
}
Ugao & Ugao::SaberiSa(const Ugao & u){
        Ugao temp;

        temp.Postavi(_radijani+u.DajRadijane());
        return temp;


}
Ugao & Ugao::PomnoziSa(double x){
        Ugao temp;
        temp.Postavi(_radijani*x);
        return temp;
}
