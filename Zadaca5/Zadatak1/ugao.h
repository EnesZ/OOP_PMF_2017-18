#ifndef UGAO_H
#define UGAO_H
#include <iostream>

extern const double PI;
class  Ugao{

private:

    double _radijani;

public:

    Ugao(double radijani = 0);
    Ugao(int stepeni, int minute, int sekunde);
    void Postavi(double radijani);
    void Postavi(int stepeni, int minute, int sekunde);
    double DajRadijane() const;
    void OcitajKlasicneJedinice(int & stepeni, int & minute, int &sekunde);
    int DajStepene() const;
    int DajMinute() const;
    int DajSekunde() const;
    void Ispisi() const;
    void IspisiKlasicno() const;
    Ugao & SaberiSa(const Ugao & u);
    Ugao & PomnoziSa(double x);

};

#endif // _UGAO_H
