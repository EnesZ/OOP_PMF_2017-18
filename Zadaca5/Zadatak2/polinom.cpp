#include <iostream>
#include <vector>
#include <string>
#include "polinom.h"




Polinom::Polinom(int stepen):stepen_(stepen){}
Polinom::Polinom():stepen_(0){}
double Polinom::operator[](int i){
        if(i>=koeficijenti_.size())
            throw std::string("Greska. Koeficijent koji ste trazili ne postoji");
        return koeficijenti_.at(i);

}
double Polinom::operator()(double x){
        double suma=0;
        double temp=1;
        for(int i=0;i<koeficijenti_.size();i++){
            suma+=(koeficijenti_.at(i)*temp);
            temp*=x;
        }
        return suma;
}
Polinom Polinom::operator+(const Polinom& p){
        Polinom q;
        if(stepen_>p.stepen_)
            q.stepen_=stepen_;
        else
            q.stepen_=p.stepen_;

        int i=0;
        while(i<koeficijenti_.size() && i<p.koeficijenti_.size()){
            q.koeficijenti_.push_back(koeficijenti_.at(i)+p.koeficijenti_.at(i));
            i++;
        }
        while(i<koeficijenti_.size()){
            q.koeficijenti_.push_back(koeficijenti_.at(i));
            i++;
        }
        while(i<p.koeficijenti_.size()){
            q.koeficijenti_.push_back(p.koeficijenti_.at(i));
            i++;
        }
        return q;

}
Polinom Polinom::operator-(const Polinom& p){
        Polinom q;
        if(stepen_>p.stepen_)
            q.stepen_=stepen_;
        else
            q.stepen_=p.stepen_;

        int i=0;
        while(i<koeficijenti_.size() && i<p.koeficijenti_.size()){
            q.koeficijenti_.push_back(koeficijenti_.at(i)-p.koeficijenti_.at(i));
            i++;
        }
        while(i<koeficijenti_.size()){
            q.koeficijenti_.push_back(koeficijenti_.at(i));
            i++;
        }
        while(i<p.koeficijenti_.size()){
            q.koeficijenti_.push_back((-1)*p.koeficijenti_.at(i));
            i++;
        }
        return q;

}
Polinom Polinom::operator-(){
        Polinom q;
        q.stepen_=stepen_;

        for(int i=0;i<koeficijenti_.size();i++)
            q.koeficijenti_.push_back((-1)*koeficijenti_.at(i));

        return q;
}
bool Polinom::operator==(const Polinom& p){
        if(p.koeficijenti_.size()!=koeficijenti_.size())
            return false;

        for(int i=0;i<p.koeficijenti_.size();i++)
            if(p.koeficijenti_.at(i)!=koeficijenti_.at(i))
                return false;

        return true;

}
bool Polinom::operator!=(const Polinom& p){
        if(p.koeficijenti_.size()!=koeficijenti_.size())
            return true;

        for(int i=0;i<p.koeficijenti_.size();i++)
            if(p.koeficijenti_.at(i)!=koeficijenti_.at(i))
                return true;

        return false;

}


std::ostream& operator<<(std::ostream& izlaz,const Polinom& p){
        int temp=1;
        bool prvi_u_zapisu=true;
        if(p.koeficijenti_.size()>0 && p.koeficijenti_.at(0)!=0){
            if(p.koeficijenti_.at(0)>0)
                izlaz<<p.koeficijenti_.at(0);
            else
                izlaz<<p.koeficijenti_.at(0);

            prvi_u_zapisu=false;
        }

        for(int i=1;i<p.koeficijenti_.size();i++){

            if(p.koeficijenti_.at(i)!=0){

                if(prvi_u_zapisu){
                    if(p.koeficijenti_.at(i)==1)
                        izlaz<<"x^"<<temp;
                    else if(p.koeficijenti_.at(i)==-1)
                        izlaz<<'-'<<"x^"<<temp;
                    else if(p.koeficijenti_.at(i)>0)
                        izlaz<<p.koeficijenti_.at(i)<<"x^"<<temp;
                    else
                        izlaz<<p.koeficijenti_.at(i)<<"x^"<<temp;

                    prvi_u_zapisu=false;
                }else{
                    if(p.koeficijenti_.at(i)==1)
                        izlaz<<'+'<<"x^"<<temp;
                    else if(p.koeficijenti_.at(i)==-1)
                        izlaz<<'-'<<"x^"<<temp;
                    else if(p.koeficijenti_.at(i)>0)
                        izlaz<<'+'<<p.koeficijenti_.at(i)<<"x^"<<temp;
                    else
                        izlaz<<p.koeficijenti_.at(i)<<"x^"<<temp;
                }
            }
            temp++;
        }
        return izlaz;

}
