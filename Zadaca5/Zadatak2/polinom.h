#ifndef _POLINOM_H
#define _POLINOM_H
#include <iostream>
#include <vector>


class Polinom{
public:
    int stepen_;
    std::vector<double> koeficijenti_;

    explicit Polinom(int stepen);
    explicit Polinom();
    double operator[](int i);
    double operator()(double x);
    Polinom operator+(const Polinom& p);
    Polinom operator-(const Polinom& p);
    Polinom operator-();
    bool operator==(const Polinom& p);
    bool operator!=(const Polinom& p);

};
std::ostream& operator<<(std::ostream& izlaz,const Polinom& p);

#endif // _POLINOM_H
