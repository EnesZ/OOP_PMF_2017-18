#include <iostream>
#include <vector>
#include "polinom.h"

using namespace std;


int main()
{

    Polinom p(5);
    p.koeficijenti_.push_back(-1);
    p.koeficijenti_.push_back(0);
    p.koeficijenti_.push_back(3);
    p.koeficijenti_.push_back(0);
    p.koeficijenti_.push_back(-100);

    cout<<p<<endl;


    try{
        cout<<"Slobodni clan je: "<<p[0]<<endl;
        cout<<"10. koeficijent je: ";
        cout<<p[10]<<endl;
    }catch(string s){
        cout<<s<<endl;
    }

    cout<<"P(0)="<<p(0)<<endl<<"P(1)="<<p(1)<<endl;

    cout<<"Polinom sa obrnutim predznacima je"<<endl<<-p<<endl;

    Polinom q(3);
    q.koeficijenti_.push_back(1);
    q.koeficijenti_.push_back(1);
    q.koeficijenti_.push_back(1);

    cout<<endl<<p<<" + "<<q<<" = "<<p+q<<endl;
    cout<<endl<<p<<" -("<<q<<") = "<<p-q<<endl;

    bool b1=(p==p);
    bool b2=(p!=p);
    cout<<"p==p: "<<b1<<" p!=p: "<<b2;
    return 0;
}
