#include <iostream>
#include <vector>
#include <string>
#include <math.h>


using namespace std;

struct Parametri{

    double x1,x2,y1,y2;
    int sirina,visina;

};

const char OKVIR='#';

vector<string> funkcijaSin(Parametri const &par, char c='*');
vector<string> funkcijaCos(Parametri const &par, char c='*');
vector<string> funkcija(Parametri const &par, char c='*');
void ispisFunkcije(vector<string> const &vec);
vector<string> spojiFunkcije(vector<string> const &vec1, vector<string> const &vec2);

int main()
{
    Parametri parametri;
    do{
        cout<<"Unesite sirinu i visinu dijagrama\n(u sirinu i visinu se ubraja i okvir, zbog preglednosti sirina je max 80)"<<endl;
        cout<<"Sirina: ";
        cin>>parametri.sirina;
        cout<<"Visina: ";
        cin>>parametri.visina;
    }while(parametri.sirina<1 || parametri.visina <1);

    //zbog okvira
    parametri.sirina-=2;
    parametri.visina-=2;

    do{
        cout<<"Unesite granice za segment [x1,x2]\nx1: ";
        cin>>parametri.x1;
        cout<<"x2: ";
        cin>>parametri.x2;
    }while(parametri.x1>=parametri.x2);

    do{
        cout<<"Unesite granice za segment [y1,y2]\ny1: ";
        cin>>parametri.y1;
        cout<<"y2: ";
        cin>>parametri.y2;
    }while(parametri.y1>=parametri.y2);


    vector<string> f1,f2,f3;

    f1=funkcijaSin(parametri);
    ispisFunkcije(f1);

    f2=funkcijaCos(parametri);
    ispisFunkcije(f2);

    f3=funkcija(parametri);
    ispisFunkcije(f3);

    f1=spojiFunkcije(f1,f2);
    f1=spojiFunkcije(f1,f3);
    ispisFunkcije(f1);


    return 0;
}

vector<string> funkcijaSin(Parametri const &par, char c/* ='*'  */){

    vector<string> ispis;
    string red;
    double vrijednost_funkcije;

    //korak
    double dx=(par.x2-par.x1)/par.sirina;
    double dy=(par.y2-par.y1)/par.visina;

    string temp(par.sirina+2,OKVIR);
    ispis.push_back(temp);

     //da se bolje vidi funkcija, konkretno za skup parametara 3 iz zadace 1 (sada samo kopirano)
    double podebljanje=0.;
    if(dy<=0.05)
        podebljanje+=dy;


    for(double i=par.y2; i>par.y1; i-=dy){

        red=OKVIR;

            /*
            *   Zbog ogranicene preciznosti koraka dx moze se desiti da program ispise kolonu vise
            *   U slucaju sirine 80, potpuni izgled dijagrama ce se poremeti
            *   Zbog toga uvodimo varijablu temp_brojac koja ce osigurati da broj kolona bude pocetna sirina-2
            *
            *   Naravno, da je program ispravan i bez dodatne varijable temp_brojac moze se provjeriti
            *   za sirinu 80 i recimo x1=-38 , x2=40 tako da je dx=1
            *
            *   (samo kopirano iz zadace 1)
            */

        int temp_brojac=0;

        for(double j=par.x1; j<par.x2; j+=dx){

            temp_brojac++;
            if(temp_brojac>par.sirina)
                break;

            vrijednost_funkcije= sin(j);

            if(i-dy<=vrijednost_funkcije && vrijednost_funkcije <i+podebljanje)
                red+=c;
            else if((i-dy<=0 && 0<i)&&(j<0 && 0<=j+dx))
                red+='+';
            else if(i-dy<=0 && 0<i)
                red+='-';
            else if(j<0 && 0<=j+dx)
                red+='|';
            else
                red+=' ';
        }

        red+=OKVIR;

        ispis.push_back(red);

    }

    ispis.push_back(temp);
    return ispis;
}

vector<string> funkcijaCos(Parametri const &par, char c/* ='*'  */){

    vector<string> ispis;
    string red;
    double vrijednost_funkcije;

    //korak
    double dx=(par.x2-par.x1)/par.sirina;
    double dy=(par.y2-par.y1)/par.visina;

    string temp(par.sirina+2,OKVIR);
    ispis.push_back(temp);

     //da se bolje vidi funkcija, konkretno za skup parametara 3 iz zadace 1 (sada samo kopirano)
    double podebljanje=0.;
    if(dy<=0.05)
        podebljanje+=dy;


    for(double i=par.y2; i>par.y1; i-=dy){

        red=OKVIR;

            /*
            *   Zbog ogranicene preciznosti koraka dx moze se desiti da program ispise kolonu vise
            *   U slucaju sirine 80, potpuni izgled dijagrama ce se poremeti
            *   Zbog toga uvodimo varijablu temp_brojac koja ce osigurati da broj kolona bude pocetna sirina-2
            *
            *   Naravno, da je program ispravan i bez dodatne varijable temp_brojac moze se provjeriti
            *   za sirinu 80 i recimo x1=-38 , x2=40 tako da je dx=1
            *
            *   (samo kopirano iz zadace 1)
            */

        int temp_brojac=0;

        for(double j=par.x1; j<par.x2; j+=dx){

            temp_brojac++;
            if(temp_brojac>par.sirina)
                break;

            vrijednost_funkcije= cos(j);

            if(i-dy<=vrijednost_funkcije && vrijednost_funkcije <i+podebljanje)
                red+=c;
            else if((i-dy<=0 && 0<i)&&(j<0 && 0<=j+dx))
                red+='+';
            else if(i-dy<=0 && 0<i)
                red+='-';
            else if(j<0 && 0<=j+dx)
                red+='|';
            else
                red+=' ';
        }

        red+=OKVIR;

        ispis.push_back(red);

    }

    ispis.push_back(temp);
    return ispis;

}

vector<string> funkcija(Parametri const &par, char c/* ='*'  */){

    vector<string> ispis;
    string red;
    double vrijednost_funkcije;

    //korak
    double dx=(par.x2-par.x1)/par.sirina;
    double dy=(par.y2-par.y1)/par.visina;

    string temp(par.sirina+2,OKVIR);
    ispis.push_back(temp);

     //da se bolje vidi funkcija, konkretno za skup parametara 3 iz zadace 1 (sada samo kopirano)
    double podebljanje=0.;
    if(dy<=0.05)
        podebljanje+=dy;


    for(double i=par.y2; i>par.y1; i-=dy){

        red=OKVIR;

            /*
            *   Zbog ogranicene preciznosti koraka dx moze se desiti da program ispise kolonu vise
            *   U slucaju sirine 80, potpuni izgled dijagrama ce se poremeti
            *   Zbog toga uvodimo varijablu temp_brojac koja ce osigurati da broj kolona bude pocetna sirina-2
            *
            *   Naravno, da je program ispravan i bez dodatne varijable temp_brojac moze se provjeriti
            *   za sirinu 80 i recimo x1=-38 , x2=40 tako da je dx=1
            *
            *   (samo kopirano iz zadace 1)
            */

        int temp_brojac=0;

        for(double j=par.x1; j<par.x2; j+=dx){

            temp_brojac++;
            if(temp_brojac>par.sirina)
                break;

            vrijednost_funkcije= (-3)*j*j;

            if(i-dy<=vrijednost_funkcije && vrijednost_funkcije <i+podebljanje)
                red+=c;
            else if((i-dy<=0 && 0<i)&&(j<0 && 0<=j+dx))
                red+='+';
            else if(i-dy<=0 && 0<i)
                red+='-';
            else if(j<0 && 0<=j+dx)
                red+='|';
            else
                red+=' ';
        }

        red+=OKVIR;

        ispis.push_back(red);

    }

    ispis.push_back(temp);
    return ispis;
}

void ispisFunkcije(vector<string> const &vec){

    for(int i=0;i<vec.size();i++){
        cout<<vec.at(i);

        if(vec.at(0).size()<80)
            cout<<endl;
        //else sam ce ispisati novi red

    }

}

vector<string> spojiFunkcije(vector<string> const &vec1, vector<string> const &vec2){

    vector<string> ispis=vec1;
    string red;

    for(int i=0;i<vec2.size();i++){

        red=vec2.at(i);

        for(int j=0;j<red.size();j++){

            // Pretpostavimo da funkcija nije iscrtana sa znakovima ' ','-','+','|' i OKVIR jer se oni koriste za druge svrhe

            if(red.at(j)==' ' || red.at(j)=='-' || red.at(j)=='+'
                || red.at(j)=='|' || red.at(j)==OKVIR)
                    continue;
            else
                ispis.at(i).at(j)=red.at(j);
        }

    }

    return ispis;
}


